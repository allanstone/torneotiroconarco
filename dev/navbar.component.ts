import { Component, OnInit } from '@angular/core';
import { ROUTER_DIRECTIVES } from "@angular/router-deprecated";
import {MaterializeDirective} from "angular2-materialize";
import {ControlGroup} from '@angular/common';
import {FormBuilder} from '@angular/common';
import {Validators} from '@angular/common';
import {TorneoService} from './torneo.service';



@Component({
	selector: 'nav-bar',
	templateUrl: 'src/templates/nav-bar.html',
	directives: [ROUTER_DIRECTIVES, MaterializeDirective],
	providers: [TorneoService ]
})

export class NavBarComponent {
	private torneo: TorneoService;
	name: string;
	active: string;
	public loginForm: ControlGroup;

	/**
	 * init name of application and active link for navbar
	 */
	constructor( 
		private _formBuilder: FormBuilder, 
		private _TorneoService: TorneoService){
		this.torneo = _TorneoService;
		this.name = "CDMA";
		this.active = "Home";
	}

	/**
	 * active for navbarlinks
	 * @param {string} active : receive value for current active link
	 */
	setActive(active: string){
		this.active = active;
		// console.log(active);
	}

	login(form){
		// console.log(form);
		var self = this;
		var response=self.torneo.makeLogin(form);
		console.log(response);
		self.loginForm.controls['Username'].updateValue("");
		self.loginForm.controls['Password'].updateValue("");
	}
	ngOnInit(): any{
		this.loginForm = this._formBuilder.group({
			'Username': ['',Validators.required],
			'Password': ['',Validators.required]
		});
	}

}