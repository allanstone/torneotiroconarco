import { Component } from '@angular/core';
import {TorneoService} from './torneo.service';


@Component({
	templateUrl:'src/templates/admin.html',
    providers: [TorneoService]
})

export class AdminComponent{
    private torneo: TorneoService;
    public listReferies: Array<any>=[];
    partido: number;
    ronda: number;
    eq1: string;
    eq2: string;
    paca: string;


	constructor(private _TorneoService: TorneoService){
        this.torneo = _TorneoService;
        this.partido=1;
        this.ronda=1;
        this.eq1="Uam";
        this.eq2="UVM";
		this.paca="2";
        this.listReferies = this.torneo.getReferies();
        console.log(this.listReferies);
	}

    nextPartido(){
        this.partido++;
        this.ronda=1;
        this.eq1="Unam";
        this.eq2="Ibero";
        this.paca="4";
    }

}