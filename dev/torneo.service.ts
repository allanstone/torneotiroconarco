import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import 'rxjs/Rx';
// Registro para
// Jugador, Equipo, Arbitro y Paca

@Injectable()
export class TorneoService implements OnInit{
	private listPlayer: Array<any>=[];
	private listTeam: Array<any>=[];
	private listReferi: Array<any>=[];
	private listPaca: Array<any>=[];
	private listUnivesity: Array<any>=[];
	private users: Array<any>=[];
	constructor(
		private _http: Http){
		var self = this;
		self.users = [
			{
				Username: "Margarito",
				Password: "margaro"
			},
			{
				Username: "Tanya",
				Password: "tania"
			},
		];

		self.listReferi = [
			{
				Clave: 1,
				Nombre: "Referi1",
				Apellidos: "ReferiAp"
			},
			{
				Clave: 2,
				Nombre: "Referi2",
				Apellidos: "Apellidos"
			}
		];

		self.listUnivesity = [
			{
				Clave: 1,
				Direccion: "Prolongación Paseo de la Reforma 880, Alvaro Obregon, Lomas de Sta Fé, 01219 Ciudad de México, D.F.",
				Nombre: "Ibero",
				teamsCount: 2
			},
			{
				Clave: 2,
				Direccion: "Prol. Canal de Miramontes 3855, Tlalpan, Ex Hacienda San Juan de Dios, 14387 Ciudad de México, D.F.",
				Nombre: "UAM",
				teamsCount: 2
			}
		];

		self.listTeam = [
			{
				Clave: 1,
				Nombre: "EquipoUam1",
				Representante: "UAM",
				Universidad: "2",
				playersCount:2
			},
			{
				Clave: 2,
				Nombre: "EquipoUam2",
				Representante: "UAM",
				Universidad: "2",
				playersCount:0
			},
			{
				Clave: 3,
				Nombre: "EquipoIbero1",
				Representante: "Ibero",
				Universidad: "1",
				playersCount:1
			},
			{
				Clave: 4,
				Nombre: "EquipoIbero2",
				Representante: "Ibero",
				Universidad: "1",
				playersCount:0
			},
		];

		self.listPlayer = [
			{
				Clave: 1,
				Nombre: "Juagador1 UAM", 
				Apellidos: "Apellidos", 
				Equipo: "1"
			},
			{
				Clave: 2,
				Nombre: "Juagador2 UAM", 
				Apellidos: "Apellidos2", 
				Equipo: "1"
			},
			{
				Clave: 3,
				Nombre: "Juagador1 Ibero", 
				Apellidos: "Apellidos", 
				Equipo: "3"
			}
		]

	}

	public getUniversities(){
		return this.listUnivesity;
	}
	public getTeams(){
		return this.listTeam;
	}
	public getReferies(){
		return this.listReferi;
	}
	
	public getPlayers(){
			return this.listPlayer;
		}

	public makeLogin(userObj){
		var confirm = _.filter(this.users, function(u) { 
			if(u.Username == userObj.Username && u.Password == userObj.Password){
				return u;
			}
		 });
	}

}