import { Component } from '@angular/core';
import {MaterializeDirective} from "angular2-materialize";
import {TorneoService} from './torneo.service';

@Component({
	templateUrl:'src/templates/calendar.html',
    directive: [MaterializeDirective],
    providers: [TorneoService]
})

export class CalendarComponent{
    datepicker: string;
    contructor(){
         this.datepicker = '01/02/2016';

    }
}