import {Component} from '@angular/core';
import {CORE_DIRECTIVES, FORM_DIRECTIVES} from '@angular/common';

@Component({
	selector: 'carousel',
	templateUrl: 'src/templates/carousel.html',
	styleUrls:['bower_components/magic/magic.css'],
	directives:[CORE_DIRECTIVES, FORM_DIRECTIVES]
})

export class CarouselComponent{
	public myInterval:number = 4000;
	public noWrapSlides:boolean = false;
	public Active: number;
	public carouselObj:Array<any> = [];
	
	constructor(){
		this.carouselObj.push(
			{
				id: 1,
				name: "img 1",
				src: "src/img/img1.jpg"
			},
			{
				id: 2,
				name: "img 2",
				src: "src/img/img2.jpg"
			},
			{
				id: 3,
				name: "img 3",
				src: "src/img/img3.jpg"
			}
		);
		this.Active=1;
		// console.log(this.carouselObj.length);
		setInterval( () => { this.timeActive(this.carouselObj.length); }, this.myInterval);

	}

	timeActive(max){
		if(this.Active >= max) {
			this.Active=1;
		}else{
			this.Active++;
		}
		// console.log(this.Active);
	}

	
}