import {Component} from '@angular/core';

@Component({
	templateUrl: 'src/templates/results.html'
})

export class ResultComponent{
	title: string;

	constructor(){
		this.title = "Results";
	}
}