import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { ROUTER_DIRECTIVES } from "@angular/router-deprecated";
import { RouteConfig } from "@angular/router-deprecated";
import { RegisterComponent } from './register.component';
import { CalendarComponent } from './calendar.component';
import { WelcomeComponent } from './welcome.component';
import { TorneoService } from './torneo.service';
import { ResultComponent } from './result.component';
import { NavBarComponent } from './navbar.component';
import { AdminComponent } from './admin.component';

@Component({
    selector: 'my-app',
    templateUrl: 'src/templates/my-app.html',
    directives: [ROUTER_DIRECTIVES, NavBarComponent],
    providers: [
    	TorneoService
    ]
})

@RouteConfig([
	{ path: '/', 		 name: 'Home', 		component: WelcomeComponent },
	{ path: '/register', name: 'Register', 	component: RegisterComponent },
	{ path: '/calendar', name: 'Calendar', 	component: CalendarComponent },
	{ path: '/results', name: 'Results', 	component: ResultComponent },
	{ path: '/admin', name: 'Admin', 	component: AdminComponent }
])

export class AppComponent implements OnInit {
	public Days1;
	public Days2;
	public Hours1;
	public Hours2;
	public Minutes1;
	public Minutes2;
	public Seconds1;
	public Seconds2;
	public Start;
	public endCountdown;

	/**
	 * initialize the countdown
	 * Date(year,month-1,day,hours,minutes,seconds,miliseconds)
	 */
	constructor(private service: TorneoService){
		var self = this;
		self.endCountdown = false;
		self.Start = new Date(2016, 4, 24, 18, 0);
		setInterval(function () {
			var date=self.getTimeRemaining(self.Start);
			if(date.days == 0 && date.hours == 0 && date.minutes==0 && date.seconds == 0) {
				self.endCountdown=true;
			}
			var Day = date.days;
			self.Days1 = Math.trunc(Day/10);
			self.Days2 = Day%10;
			var Hours = date.hours;
			self.Hours1 = Math.trunc(Hours/10);
			self.Hours2 = Hours%10;
			var Minutes = date.minutes;
			self.Minutes1 = Math.trunc(Minutes/10);
			self.Minutes2 = Minutes%10;
			var Seconds = date.seconds;
			self.Seconds1 = Math.trunc(Seconds/10);
			self.Seconds2 = Seconds%10;
		},1000);
	}

	 getTimeRemaining(endtime) {
		  var t = Date.parse(endtime) - Date.parse(new Date());
		  var seconds = Math.floor((t / 1000) % 60);
		  var minutes = Math.floor((t / 1000 / 60) % 60);
		  var hours = Math.floor((t / (1000 * 60 * 60)) % 24);
		  var days = Math.floor(t / (1000 * 60 * 60 * 24));
		  return {
		    'total': t,
		    'days': days,
		    'hours': hours,
		    'minutes': minutes,
		    'seconds': seconds
		  };
	}
	
	OnInit(){
	}

}