import {Component} from '@angular/core';
import {OnInit} from '@angular/core';
import {ControlGroup} from '@angular/common';
import {FormBuilder} from '@angular/common';
import {Validators} from '@angular/common';
import {TorneoService} from './torneo.service';

// Registro para
// Jugador, Equipo, Arbitro y Paca

@Component({
	templateUrl:'src/templates/register.html',
	providers: [TorneoService]
})

export class RegisterComponent{
	private torneo: TorneoService;
	public maxPlayers: number;
	public maxTeamsUnivesity: number;
	public maxTeams: number;
	public TeamNotParticip: string;
	public teamMsj: string;

	public showForm: string;
	public listPlayer: Array<any>=[];
	public listTeam: Array<any>=[];
	public listReferi: Array<any>=[];
	public listPaca: Array<any>=[];
	public listUnivesity: Array<any>=[];

	public playerForm: ControlGroup;
	public teamForm: ControlGroup;
	public referiForm: ControlGroup;
	public universityForm: ControlGroup;
	public pacaForm: ControlGroup;

	constructor(private _formBuilder: FormBuilder, 
		private _TorneoService: TorneoService){
		this.torneo = _TorneoService;
		// 1. Se podrán registrar hasta 32 equipos
		this.maxTeams = 32;

		// 2. Cada Universidad podrá inscribir a un máximo de 3 equipos
		this.maxTeamsUnivesity = 3;

		this.maxPlayers = 4;
		this.TeamNotParticip = "El equipo no participara sin suficientes jugadores";
		this.showForm = 'University';

	}

	savePlayer(player){
		// console.log(player);
		// player.Clave = this.listPlayer.length+1;
		player.Clave = this.listPlayer.length+1;
		this.listPlayer.push(player);
		this.addTeamPlayer(this.playerForm.controls['Equipo'].value);
		this.playerForm.controls['Nombre'].updateValue("");
		this.playerForm.controls['Apellidos'].updateValue("");
		this.playerForm.controls['Equipo'].updateValue("");
		// this.playerForm.value ={};
	}
	
	addTeamPlayer(TeamId){
		this.listTeam = _.filter(this.listTeam, function (t) {
			if(t.Clave == TeamId){
				t.playersCount=++t.playersCount;
			}
			return t;
		});
		// console.log(this.listTeam);
	}

	addUnivesityTeam(univesityId){
		this.listUnivesity = _.filter(this.listUnivesity, function (u) {
			if(u.Clave == univesityId){
				u.teamsCount=++u.teamsCount;
			}
			return u;
		});
		// console.log(this.listUnivesity);
	}
	

	saveTeam(team){
		if(this.listTeam.length+1 >= this.maxTeams) {
			this.teamMsj = "Error numero máximo de equipos excedido";
			return true;
		}
		team.Clave = this.listTeam.length+1;
		team.playersCount = 0;
		this.listTeam.push(team);
		console.log(this.listTeam);
		this.addUnivesityTeam(this.teamForm.controls['Universidad'].value);
		this.teamForm.controls['Nombre'].updateValue("");
		this.teamForm.controls['Representante'].updateValue("");
		this.teamForm.controls['Universidad'].updateValue("");		
		// console.log(team);
	}

	saveReferi(referi){
		referi.Clave = this.listReferi.length+1;
		this.listReferi.push(referi);
		this.referiForm.controls['Nombre'].updateValue("");
		this.referiForm.controls['Apellidos'].updateValue("");
		// console.log(referi);
	}

	savePaca(paca){
		paca.Clave = this.listPaca.length+1;
		this.listPaca.push(paca);
		this.pacaForm.controls['Nombre'].updateValue("");
		this.pacaForm.controls['Lugar'].updateValue("");
		// console.log(paca);
	}

	saveUniversity(university){		
		university.Clave = this.listUnivesity.length+1;
		// defect value when is created
		university.teamsCount = 0;
		this.listUnivesity.push(university);
		// console.log(this.listUnivesity);
		this.universityForm.controls['Nombre'].updateValue("");
		this.universityForm.controls['Direccion'].updateValue("");
		// console.log(university);
	}

	setForm(name: string){
		this.showForm = name;
		// console.log(name);
	}

	ngOnInit(): any{
		this.listUnivesity = this.torneo.getUniversities();
		this.listTeam= this.torneo.getTeams();
		this.listPlayer= this.torneo.getPlayers();

		this.universityForm = this._formBuilder.group({
			'Nombre': ['',Validators.required],
			'Direccion': ['',Validators.required]
		});

		this.teamForm = this._formBuilder.group({
			'Nombre': ['',Validators.required],
			'Universidad': ['',Validators.required], 
			'Representante': ['',Validators.required]
		});

		this.playerForm = this._formBuilder.group({
			'Nombre': ['',Validators.required],
			'Apellidos': ['',Validators.required],
			'Equipo': ['',Validators.required]
		});


		this.referiForm = this._formBuilder.group({
			'Nombre': ['',Validators.required],
			'Apellidos': ['',Validators.required]
		});
		
		this.pacaForm = this._formBuilder.group({
			'Nombre': ['',Validators.required],
			'Lugar': ['',Validators.required]
		});


	}
}