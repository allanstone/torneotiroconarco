import {Component} from '@angular/core';
import {CarouselComponent} from './carousel.component';

@Component({
	templateUrl:'src/templates/welcome.html',
	directives: [CarouselComponent]
})

export class WelcomeComponent{
	title: string;

	constructor(){
		this.title = "Welcome";
	}
}